<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * @property int $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $email
 * @property string $phone
 * @property string|null $text
 * @property int|null $manager_id
 * @property int|null $double
 *
 * @property Manager|null $manager
 */
class Request extends ActiveRecord
{


    public static function tableName(): string
    {
        return 'requests';
    }

    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    public function rules()
    {
        return [
            [['email', 'phone'], 'required'],
            ['email', 'email'],
            ['manager_id', 'integer'],
            ['manager_id', 'exist', 'targetClass' => Manager::class, 'targetAttribute' => 'id'],
            [['email', 'phone'], 'string', 'max' => 255],
            ['text', 'safe'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'created_at' => 'Добавлен',
            'updated_at' => 'Изменен',
            'email' => 'Email',
            'phone' => 'Номер телефона',
            'manager_id' => 'Ответственный менеджер',
            'text' => 'Текст заявки',
            'double' => 'Предыдущая заявка',
        ];
    }

    public function getManager(): ActiveQuery
    {
        return $this->hasOne(Manager::class, ['id' => 'manager_id']);
    }

    public function beforeSave($insert): bool
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->assignManager($this);
            }
            return true;
        }
        return false;
    }

    public function assignManager($request): void
    {
        $previousRequest = $this->getPreviousRequests();

        if ($previousRequest && $previousRequest->manager->is_works) {
            $request->manager_id = $previousRequest->manager_id;
        } else {
            $managers = Manager::find()->where(['is_works' => true])->all();
            $manager = $managers[array_rand($managers)];
            $request->manager_id = $manager->id;
        }
    }

    public function getPreviousRequests(): array
    {
        return self::find()
            ->where(['or', ['email' => $this->email], ['phone' => $this->phone]])
            ->andWhere(['<', 'created_at', $this->created_at])
            ->andWhere(['>', 'created_at', strtotime('-30 days')])
            ->orderBy(['created_at' => SORT_DESC])
            ->all();
    }
}
